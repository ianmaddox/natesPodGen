#!/usr/bin/env python3
import os, io
import shutil
from configupdater import ConfigUpdater
import re
import datetime
import time
import dateutil.parser
import argparse
from mutagen.mp3 import MP3
parser = argparse.ArgumentParser(description="add an entry to feed.ini")
parser.add_argument('mp3file', type=str , nargs=1 , help='The mp3 file to be added to your feed')
parser.add_argument('-i','--image',type=str,nargs=1,default = None ,help='the optional episode specific image for your episode')
parser.add_argument('-c','--category',type=str,nargs='*',default = " " ,help='iTunes Category')
parser.add_argument('-k','--keywords',type=str,nargs='*',default = " " ,help='iTunes Keywords (separated by commas)')
args = parser.parse_args()
mp3filepath = vars(args)['mp3file'][0]
workingPath = os.path.realpath("/".join(__file__.split('/')[:-1]))
print(mp3filepath)

def calclength(mp3filepath):
    mp3len = MP3(mp3filepath).info.length
    mp3sec = str(int(mp3len % 60)).zfill(2)
    mp3min = str(int(mp3len/60 % 60)).zfill(2)
    mp3hr = str(int(mp3len/3600 % 3600)).zfill(2)
    mp3len = mp3hr + ":" + mp3min + ":" + mp3sec
    return mp3len

def getnextepisodeid():
    updater = ConfigUpdater()
    updater.read('feed.ini')
    return updater['global']['nextEpisodeID'].value

def incrementnextepisodeid():
    updater = ConfigUpdater()
    updater.read('feed.ini')
    updater['global']['nextEpisodeID'].value = int(updater['global']['nextEpisodeID'].value) + 1
    updater.update_file()

def askforreleasedate():
    validMonths = ('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec')
    answer = input('Set release date to today?[y,N]: ')
    answer = "".join(answer).lower()
    if answer == 'y':
        today = datetime.date.fromtimestamp(time.time())
        return today.strftime('%Y %b %d')
    else:
        rawdate = input("Release date: ")
        date = dateutil.parser.parse(rawdate)
        while not date:
            rawdate = input("Release date (YYYY-MM-DD): ")
            date = dateutil.parser.parse(rawdate)
        return date.strftime("%Y %b %d").lower()

image = None
uniqueid = str(getnextepisodeid())
duration = calclength(mp3filepath)
print("Episode " + uniqueid + "  duration:" + duration)

title = input("Title: ")
description = input("Description: ")
title = str(uniqueid) + ':' + title

if vars(args)['category'] is not None:
    itunesCategory = " ".join(vars(args)['category'])
    print("Category: " + itunesCategory)
else:
    itunesCategory = input("itunes Category")

if vars(args)['keywords'] is not None:
    itunesKeywords = ",".join(vars(args)['keywords'])
    print("Keywords: " + itunesKeywords)
else:
    itunesKeywords = input("itunes Keywords(separated by commas)")

releaseDate = askforreleasedate()

link = 'episodes/episode-' + uniqueid.zfill(4) + '.mp3'
print('copying the mp3 file to' + workingPath + '/site/' + link)
shutil.copy(mp3filepath, workingPath + '/site/' + link)

if vars(args)['image'] is not None:
    image = args.image[0]
    fileExt = os.path.splitext(image)[1]
    imagePath = 'images/episode-' + uniqueid.zfill(4) + fileExt
    internalImagePath = 'site/' + imagePath
    print('copying the image file to' + workingPath+ '/' + internalImagePath)
    shutil.copy(image, workingPath + '/' + internalImagePath)

incrementnextepisodeid()
with open("feed.ini",'a') as feedfile:
    feedfile.write('[' + title + ']\n')
    feedfile.write('description = ' + description + '\n')
    feedfile.write('releaseDate = ' + releaseDate + '\n')
    feedfile.write('uniqueid = ' + str(uniqueid) + '\n')
    feedfile.write('itunesCategory = ' + itunesCategory + '\n')
    feedfile.write('duration = ' + duration + '\n')
    feedfile.write('itunesKeywords = ' + itunesKeywords + '\n')
    feedfile.write('link = ' + link + '\n')
    if image is not None:
        feedfile.write('image = ' + imagePath + '\n')



